import React from 'react';
import thunk from 'redux-thunk';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

import App from './components/App';
import globalReducer from './reducers/globalReducer';

const devTools = window.devToolsExtension ? window.devToolsExtension() : f => f;
const store = createStore(globalReducer, compose(applyMiddleware(thunk), devTools));

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root'),
);
