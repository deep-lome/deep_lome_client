import axios from 'axios';
import { URL } from '../utils/constants';
import { clearLocalBasketContent } from '../actions/basket';

export const sendOrderData = (payload) => dispatch => axios
	.post(URL.SEND_ORDER_DATA, {
		headers: {
			'Content-Type': 'application/json',
		},
		body: payload,
	})
	.then(dispatch(clearLocalBasketContent()))
	.then(localStorage.setItem('basket', JSON.stringify({ content: [], size: null })))
	.catch(error => console.log(error));
