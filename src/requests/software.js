import axios from 'axios';
import { URL } from '../utils/constants';
import { fillSoftwareList } from '../actions/software';

export const getSoftwareList = () => dispatch => {
	axios
		.get(URL.SOFTWARE)
		.then(res => {
			const software = res.data.data.map(item => ({
				...item,
				url: `http://localhost:8081/public/img/${item.name.replace(/ /g, '_')}.png`,
			}));
			dispatch(fillSoftwareList(software));
			localStorage.setItem('software', JSON.stringify(software));
		})
		.catch(error => console.log(error));
};
