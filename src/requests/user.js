import axios from 'axios';
import { URL } from '../utils/constants';
import { fillUserInfo, getStatusValue, clearUserInfo } from '../actions/user';
import { changeLocalBasketContent, clearLocalBasketContent } from '../actions/basket';

export const signUp = (payload) => dispatch => axios
	.post(URL.REGISTRATION, {
		headers: {
			'Content-Type': 'application/json',
		},
		body: payload,
	})
	.then(res => (res.status !== 400) && dispatch(getStatusValue('success')))
	.catch(() => dispatch(getStatusValue('Такой логин или email уже зарегестрированы!')));

export const signOut = () => dispatch => axios
	.get(URL.SIGN_OUT, {
		headers: {
			'Content-Type': 'application/json',
			'Auth-Access': localStorage.getItem('token'),
		},
	})
	.then(dispatch(clearUserInfo()))
	.then(dispatch(clearLocalBasketContent()))
	.then(localStorage.setItem('basket', JSON.stringify({ content: [], size: null })))
	.catch(error => console.log(error));

export const signIn = (payload) => (dispatch, getState) => axios
	.post(URL.AUTHORIZATION, {
		headers: {
			'Content-Type': 'application/json',
		},
		body: payload,
	})
	.then(res => {
		localStorage.setItem('token', res.data.data.token);
		dispatch(fillUserInfo(res.data.data.userInfo));

		const userBasket = {
			content: res.data.data.userBasket,
			size: (res.data.data.userBasket.length > 0)
				? res.data.data.userBasket.map((item) => item.count)
					.reduce((first, second) => first + second) : null,
		};
		const localBasket = JSON.parse(localStorage.getItem('basket'));

		if (userBasket.content.length > 0) {
			dispatch(changeLocalBasketContent(userBasket));
			localStorage.setItem('basket', JSON.stringify(userBasket));
		} else if (localBasket.content.length > 0) {
			dispatch(changeLocalBasketContent(localBasket));
		}
	})
	.catch(() => (getState().userState.status === '') && dispatch(getStatusValue('Неправильный логин или пароль!')));

export const checkTokenActuality = () => dispatch => axios
	.get(URL.CHECK_TOKEN, {
		headers: {
			'Content-Type': 'application/json',
			'Auth-Access': localStorage.getItem('token'),
		},
	})
	.then(res => {
		dispatch(fillUserInfo(res.data.data.userInfo));

		const userBasket = {
			content: res.data.data.userBasket,
			size: (res.data.data.userBasket.length > 0)
				? res.data.data.userBasket.map((item) => item.count)
					.reduce((first, second) => first + second) : null,
		};
		const localBasket = JSON.parse(localStorage.getItem('basket'));

		if (userBasket.content.length > 0) {
			dispatch(changeLocalBasketContent(userBasket));
			localStorage.setItem('basket', JSON.stringify(userBasket));
		} else if (localBasket.content.length > 0) {
			dispatch(changeLocalBasketContent(localBasket));
		}
	})
	.catch(() => dispatch(clearUserInfo()));
