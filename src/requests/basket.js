import axios from 'axios';
import { URL } from '../utils/constants';
import { clearLocalBasketContent, changeLocalBasketContent } from '../actions/basket';

export const clearBasketContent = () => (dispatch, getState) => {
	const { authorized } = getState().userState;
	const { userName } = getState().userState.userInfo;
	const emptyBasket = { content: [], size: null };
	if (authorized) {
		axios
			.post(URL.CLEAR_BASKET_CONTENT, {
				headers: {
					'Content-Type': 'application/json',
				},
				body: { userName },
			})
			.then(localStorage.setItem('basket', JSON.stringify(emptyBasket)))
			.then(dispatch(clearLocalBasketContent()))
			.catch(error => console.log(error));
	} else {
		localStorage.setItem('basket', JSON.stringify(emptyBasket));
		dispatch(clearLocalBasketContent());
	}
};

export const changeBasketContent = (payload) => (dispatch, getState) => {
	const { authorized } = getState().userState;
	const { userName } = getState().userState.userInfo;
	if (authorized) {
		axios
			.post(URL.CHANGE_BASKET_CONTENT, {
				headers: {
					'Content-Type': 'application/json',
				},
				body: {
					userName,
					content: payload.content.map(item => ({
						code: item.product.code,
						count: item.count,
					})),
				},
			})
			.then(localStorage.setItem('basket', JSON.stringify(payload)))
			.then(dispatch(changeLocalBasketContent(payload)))
			.catch(error => console.log(error));
	} else {
		localStorage.setItem('basket', JSON.stringify(payload));
		dispatch(changeLocalBasketContent(payload));
	}
};
