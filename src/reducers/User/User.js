import { ACTIONS } from '../../utils/constants';

const initialState = {
	userInfo: {
		userName: '',
		password: '',
		firstName: '',
		lastName: '',
		phone: '',
		email: '',
		type: '',
	},
	status: '',
	authorized: false,
};

const userReducer = (state = initialState, { type, payload }) => {
	switch (type) {
	case ACTIONS.FILL_USER_INFO:
		return {
			...state,
			userInfo: payload,
			authorized: true,
		};
	case ACTIONS.GET_STATUS_VALUE:
		return {
			...state,
			status: payload,
		};
	case ACTIONS.CLEAR_USER_INFO:
		return initialState;
	default:
		return state;
	}
};

export default userReducer;
