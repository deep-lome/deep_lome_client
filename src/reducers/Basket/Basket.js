import { ACTIONS } from '../../utils/constants';

const initialState = {
	size: null,
	content: [],
};

const backetReducer = (state = initialState, { type, payload }) => {
	switch (type) {
	case ACTIONS.CHANGE_BASKET_CONTENT:
		return {
			...state,
			size: payload.size,
			content: payload.content,
		};
	case ACTIONS.CLEAR_BASKET:
		return initialState;
	default:
		return state;
	}
};

export default backetReducer;
