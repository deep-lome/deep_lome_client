import { ACTIONS } from '../../utils/constants';

const initialState = {
	search: '',
	developer: '',
	category: '',
};

const filtersReducer = (state = initialState, { type, payload }) => {
	switch (type) {
	case ACTIONS.GET_SEARCH_VALUE:
		return {
			...state,
			search: payload,
		};
	case ACTIONS.CLEAR_SEARCH_VALUE:
		return {
			...state,
			search: initialState.search,
		};
	case ACTIONS.GET_DEVELOPER_VALUE:
		return {
			...state,
			developer: payload,
		};
	case ACTIONS.CLEAR_DEVELOPER_VALUE:
		return {
			...state,
			developer: initialState.developer,
		};
	case ACTIONS.GET_CATEGORY_VALUE:
		return {
			...state,
			category: payload,
		};
	case ACTIONS.CLEAR_CATEGORY_VALUE:
		return {
			...state,
			category: initialState.category,
		};
	case ACTIONS.CLEAR_FILTERS:
		return {
			...initialState,
			search: (payload === 'search') ? state.search : '',
		};
	default:
		return state;
	}
};

export default filtersReducer;
