import { combineReducers } from 'redux';
import userReducer from './User/user';
import softwareReducer from './Software/software';
import basketReducer from './Basket/basket';
import filtersReducer from './Filters/filters';
import visibilityReducer from './Visibility/visibility';

const globalReducer = combineReducers({
	userState: userReducer,
	softwareState: softwareReducer,
	basketState: basketReducer,
	filtersState: filtersReducer,
	visibilityState: visibilityReducer,
});

export default globalReducer;
