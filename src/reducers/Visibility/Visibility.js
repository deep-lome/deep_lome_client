import { ACTIONS } from '../../utils/constants';

const initialState = {
	dialog: false,
};

const visibilityReducer = (state = initialState, { type, payload }) => {
	switch (type) {
	case ACTIONS.SHOW_MODAL_WINDOW:
		return {
			...initialState,
			[payload]: true,
		};
	case ACTIONS.HIDE_MODAL_WINDOW:
		return initialState;
	default:
		return state;
	}
};

export default visibilityReducer;
