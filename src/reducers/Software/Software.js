import { ACTIONS } from '../../utils/constants';

const initialState = {
	software: [],
};

const softwareReducer = (state = initialState, { type, payload }) => {
	switch (type) {
	case ACTIONS.FILL_SOFTWARE_LIST:
		return {
			...state,
			software: payload,
		};
	case ACTIONS.CLEAR_SOFTWARE_LIST:
		return initialState;
	default:
		return state;
	}
};

export default softwareReducer;
