import { connect } from 'react-redux';
import App from '../components/App';
import { getSoftwareList } from '../requests/software';
import { checkTokenActuality } from '../requests/user';
import { changeLocalBasketContent } from '../actions/basket';

const mapStateToProps = ({
	visibilityState: { dialog },
	userState: { authorized },
}) => ({ dialog, authorized });

const mapDispatchToProps = {
	getSoftwareList,
	checkTokenActuality,
	changeLocalBasketContent,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
