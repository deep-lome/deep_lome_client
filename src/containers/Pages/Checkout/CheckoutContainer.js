import { connect } from 'react-redux';
import Checkout from '../../../components/Pages/Checkout/Checkout';
import { sendOrderData } from '../../../requests/order';

const mapStateToProps = ({
	basketState: { content },
	userState: { userInfo },
}) => ({ content, userInfo });

const mapDispatchToProps = {
	sendOrder: sendOrderData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
