import { connect } from 'react-redux';
import SoftInfo from '../../../components/Pages/SoftInfo/SoftInfo';
import { changeBasketContent } from '../../../requests/basket';
import { showModalWindow } from '../../../actions/visibility';
import { getCategoryValue, getDeveloperValue } from '../../../actions/filters';

const mapStateToProps = ({
	softwareState: { software },
	basketState: { content, size },
}) => ({ software, content, size });

const mapDispatchToProps = {
	changeBasketContent,
	showModalWindow,
	getCategoryValue,
	getDeveloperValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(SoftInfo);
