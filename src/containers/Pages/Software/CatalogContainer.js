import { connect } from 'react-redux';
import Catalog from '../../../components/Pages/Catalog/Catalog';
import { clearFiltersList } from '../../../actions/filters';

const mapStateToProps = ({
	softwareState: { software },
	filtersState: { search, developer },
}) => ({ software, search, developer });

const mapDispatchToProps = {
	clearFiltersList,
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
