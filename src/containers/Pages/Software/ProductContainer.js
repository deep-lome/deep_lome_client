import { connect } from 'react-redux';
import Product from '../../../components/Pages/Catalog/Software/Product/Product';
import { getDeveloperValue } from '../../../actions/filters';

const mapStateToProps = ({
	basketState: { content },
}) => ({ content });

const mapDispatchToProps = {
	getDeveloper: getDeveloperValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
