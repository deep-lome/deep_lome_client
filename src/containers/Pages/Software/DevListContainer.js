import { connect } from 'react-redux';
import DevList from '../../../components/Pages/Catalog/Software/DevList/DevList';
import { getDeveloperValue, clearDeveloperValue } from '../../../actions/filters';

const mapStateToProps = ({
	softwareState: { software },
	filtersState: { developer },
}) => ({ software, developer });

const mapDispatchToProps = {
	getDeveloper: getDeveloperValue,
	clearDeveloper: clearDeveloperValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(DevList);
