import { connect } from 'react-redux';
import Search from '../../../components/Pages/Search/Search';
import { changeBasketContent } from '../../../requests/basket';
import { showModalWindow } from '../../../actions/visibility';
import { clearFiltersList } from '../../../actions/filters';

const mapStateToProps = ({
	softwareState: { software },
	basketState: { content, size },
	filtersState: { search, category, developer },
}) => ({ software, content, size, search, category, developer });

const mapDispatchToProps = {
	clearFiltersList,
	changeBasketContent,
	showModalWindow,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
