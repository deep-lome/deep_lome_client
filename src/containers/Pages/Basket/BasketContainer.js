import { connect } from 'react-redux';
import Basket from '../../../components/Pages/Basket/Basket';
import { changeBasketContent, clearBasketContent } from '../../../requests/basket';

const mapStateToProps = ({
	userState: { authorized },
	basketState: { content, size },
}) => ({ authorized, content, size });

const mapDispatchToProps = {
	changeContent: changeBasketContent,
	clearBasket: clearBasketContent,
};

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
