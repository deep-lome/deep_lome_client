import { connect } from 'react-redux';
import Registration from '../../../components/Pages/User/Profile/Profile';

const mapStateToProps = ({
	userState: { userInfo },
}) => ({ userInfo });

export default connect(mapStateToProps)(Registration);
