import { connect } from 'react-redux';
import Login from '../../../components/Pages/User/Login/Login';
import { getStatusValue } from '../../../actions/user';
import { signIn } from '../../../requests/user';

const mapStateToProps = ({
	userState: { status },
}) => ({ status });

const mapDispatchToProps = {
	signIn,
	getStatusValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
