import { connect } from 'react-redux';
import Registration from '../../../components/Pages/User/Registration/Registration';
import { getStatusValue } from '../../../actions/user';
import { signUp } from '../../../requests/user';

const mapStateToProps = ({
	userState: { status },
}) => ({ status });

const mapDispatchToProps = {
	signUp,
	getStatusValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
