import { connect } from 'react-redux';
import Header from '../../components/Header/Header';
import { getCategoryValue, getSearchValue, clearSearchValue } from '../../actions/filters';
import { signOut } from '../../requests/user';

const mapStateToProps = ({
	userState: { authorized },
	softwareState: { software },
	basketState: { size, content },
	filtersState: { search },
}) => ({ authorized, software, size, content, search });

const mapDispatchToProps = {
	signOut,
	getCategoryValue,
	getSearchValue,
	clearSearchValue,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
