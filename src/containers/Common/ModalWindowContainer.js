import { connect } from 'react-redux';
import ModalWindow from '../../components/Common/ModalWindow/ModalWindow';
import { hideModalWindow } from '../../actions/visibility';

const mapStateToProps = ({
	basketState: { content },
}) => ({ product: content[content.length - 1].product });

const mapDispatchToProps = {
	hideModal: hideModalWindow,
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalWindow);
