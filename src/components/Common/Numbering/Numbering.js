import React, { Fragment } from 'react';
import { Pagination } from 'react-bootstrap';
import styles from './Numbering.css';

const Numbering = ({ length, size, page, changePage, shadow }) => {
	const first = 1;
	const last = length;

	return (
		<Fragment>
			{(last - first > 0) && <div className={shadow ? styles.shadow : ''}>
				<Pagination size={size || ''}>
					<Pagination.Prev disabled={page < 2}
						onClick={() => changePage(page - 1)} />
					{(page >= 3) && <Pagination.Item
						onClick={() => changePage(1)}>
						{first}
					</Pagination.Item>}
					{(page >= 4 && last - first >= 6) && <Pagination.Ellipsis />}

					{(page >= last && page - first >= 5) && <Pagination.Item
						onClick={() => changePage(page - 4)}>
						{page - 4}
					</Pagination.Item>}
					{(page >= last - 1 && page - first >= 4) && <Pagination.Item
						onClick={() => changePage(page - 3)}>
						{page - 3}
					</Pagination.Item>}
					{(page >= last - 2 && page - first >= 3) && <Pagination.Item
						onClick={() => changePage(page - 2)}>
						{page - 2}
					</Pagination.Item>}

					{(page >= 2) && <Pagination.Item
						onClick={() => changePage(page - 1)}>
						{page - 1}
					</Pagination.Item>}
					<Pagination.Item active>{page}</Pagination.Item>
					{(page <= last - 1) && <Pagination.Item
						onClick={() => changePage(page + 1)}>
						{page + 1}
					</Pagination.Item>}

					{(page <= 3 && last - page >= 3) && <Pagination.Item
						onClick={() => changePage(page + 2)}>
						{page + 2}
					</Pagination.Item>}
					{(page <= 2 && last - page >= 4) && <Pagination.Item
						onClick={() => changePage(page + 3)}>
						{page + 3}
					</Pagination.Item>}
					{(page <= 1 && last - page >= 5) && <Pagination.Item
						onClick={() => changePage(page + 4)}>
						{page + 4}
					</Pagination.Item>}

					{(page <= last - 3 && last - first >= 6) && <Pagination.Ellipsis />}
					{(page <= last - 2) && <Pagination.Item
						onClick={() => changePage(last)}>
						{last}
					</Pagination.Item>}
					<Pagination.Next disabled={page > last - 1}
						onClick={() => changePage(page + 1)} />
				</Pagination>
			</div>}
		</Fragment>
	);
};

export default Numbering;
