import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import styles from './ModalWindow.css';

const ModalWindow = ({ product, hideModal }) => (
	<Fragment>
		<div className={styles.overlay} />
		<div className={styles.modal_window}>
			<div className={styles.cancel} onClick={() => hideModal('basket')} />
			<div className={styles.header}>Товар добавлен в корзину</div>
			<div className={styles.main}>
				<div className={styles.product}>
					<img className={styles.img} src={product.url} alt='Проблема с источником изображения' />
					<div className={styles.name}>{product.name}</div>
				</div>
				<div className={styles.description}>
					<div className={styles.vendor_code}>Артикул: {product.code}</div>
					<div className={styles.price_value}>
						<span className={styles.text}>Цена: </span>
						<span className={styles.value}>{product.price.toLocaleString()}</span>
						<span className={styles.text}>руб</span>
					</div>
				</div>
			</div>
			<div className={styles.footer}>
				<div className={`${styles.back} ${styles.button}`} onClick={() => hideModal('basket')}>Продолжить покупки</div>
				<Link className={`${styles.checkout} ${styles.button}`} to='/basket' onClick={() => hideModal('basket')}>
					Перейти в корзину
				</Link>
			</div>
		</div>
	</Fragment>
);

export default ModalWindow;
