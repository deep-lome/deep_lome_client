import React from 'react';
import styles from './Checkbox.css';

const Checkbox = ({ string, size, isActive, markBox }) => (
	<label className={`${styles.label} ${(size === 'big') ? styles.label_big : ''}`} onClick={markBox}>
		<span className={`${styles.checkbox} ${(size === 'small') ? styles.checkbox_small : ''}`}>
			<div className={`${styles.border} ${(size === 'small') ? styles.border_small : ''} ${(isActive) ? styles.border_checked : ''}`}>
				<div className={`${styles.mark} ${(size === 'small') ? styles.mark_small : ''} ${(isActive) ? styles.mark_checked : ''}`} />
			</div>
		</span>
		<span className={`${styles.string} ${(size === 'small') ? styles.string_small : ''}`}>{string}</span>
	</label>
);

export default Checkbox;
