import React, { useState } from 'react';
import styles from './Input.css';

const Checkout = ({
	name,
	type,
	placeholder,
	isEmpty,
	size,
	helper,
	value,
	autofocus,
	changeInputValue,
	maxlength,
	notFilled,
}) => {
	const initialState = {
		isFocused: false,
		value: '',
	};
	const [state, setState] = useState(initialState);
	const handleClick = (status) => {
		setState({
			...state,
			isFocused: status,
		});
	};

	return (
		<div className={`${styles.block} ${(size === 'big') ? styles.block_big : ''}`} onClick={() => handleClick(true)}
			onBlur={() => handleClick(false)}>
			<label id={`${name}-label`} htmlFor={name} data-shrink='false' className={`${styles.label}
			${size ? styles.label_big : ''} 
			${(state.isFocused) ? (size ? styles.label_big_focused : styles.label_focused) : ''} 
			${(state.isFocused || value) ? (size ? styles.label_big_shrink : styles.label_shrink) : ''}
			${!notFilled ? (value === '' || value.startsWith(' ')) ? (isEmpty ? styles.empty_label : '') : styles.filled_label : styles.empty_label}`}>
				{`${placeholder} *`}
			</label>
			<div className={`${size ? styles.form_big : styles.form} ${!notFilled ? (value === '' || value.startsWith(' ')) ? (isEmpty ? styles.empty_form : '') : styles.filled_form : styles.empty_form} ${(state.isFocused) ? (size ? styles.form_big_focused : styles.form_focused) : ''}`}>
				<input
					className={`${styles.input} ${size ? styles.input_big : ''}`}
					id={name}
					name={name}
					type={type}
					aria-invalid='false'
					autoComplete='off'
					required
					autoFocus={autofocus}
					value={value}
					aria-describedby={helper}
					maxLength={maxlength || 100}
					onChange={(e) => changeInputValue(e)} />
				{size && <fieldset className={`${styles.fieldset} ${(value === '' || value.startsWith(' ')) ? (isEmpty ? styles.empty : '') : styles.filled}`} aria-hidden='true'>
					<legend className={`${styles.legend} ${(state.isFocused || value) ? styles.legend_active : ''}`}>
						<span className={styles.placeholder}>{`${placeholder} *`}</span>
					</legend>
				</fieldset>}
			</div>
			{helper && <p className={styles.helper} id="cvv-helper-text">Последние три цифры на полосе подписи</p>}
		</div>
	);
};

export default Checkout;
