import React from 'react';
import styles from './Footer.css';

const Footer = () => (
	<div className={styles.footer}>
		&copy; НГТУ 2020 АВТ-610 Миронычев М.С. Дипломная работа &#171;Интернет-магазин ПО&#187;
	</div>
);

export default Footer;
