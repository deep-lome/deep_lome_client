import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import history from '../utils/history';
import Header from '../containers/Common/HeaderContainer';
import CatalogContainer from '../containers/Pages/Software/CatalogContainer';
import Login from '../containers/Pages/User/SingInContainer';
import Registration from '../containers/Pages/User/SignUpContainer';
import Profile from '../containers/Pages/User/ProfileContainer';
import Basket from '../containers/Pages/Basket/BasketContainer';
import SoftInfo from '../containers/Pages/SoftInfo/SoftInfoContainer';
import Checkout from '../containers/Pages/Checkout/CheckoutContainer';
import ModalWindow from '../containers/Common/ModalWindowContainer';
import Search from '../containers/Pages/Search/SearchContainer';
import Footer from './Footer/Footer';
import styles from './App.css';

class App extends Component {
	componentDidMount() {
		this.props.getSoftwareList();

		(history.location.pathname !== '/login' && history.location.pathname !== '/register')
			&& this.props.checkTokenActuality();

		const basket = localStorage.getItem('basket');
		!this.props.authorized && basket && this.props.changeLocalBasketContent({
			content: JSON.parse(basket).content,
			size: (JSON.parse(basket).content.length > 0)
				? JSON.parse(basket).content.map((item) => item.count)
					.reduce((first, second) => first + second) : null,
		});
	}

	render() {
		return (
			<div className={styles.app}>
				<BrowserRouter history={history}>
					<Header />
					{this.props.dialog && <ModalWindow />}
					<Switch>
						<Route exact path="/" render={() => <Redirect to='/catalog/' />} />
						<Route path="/catalog/" render={() => <CatalogContainer />} />
						<Route path="/login/" render={() => (!this.props.authorized) ? <Login /> : <Redirect to='/catalog/' />} />
						<Route path="/registration/" render={() => (!this.props.authorized) ? <Registration /> : <Redirect to='/catalog/' />} />
						<Route path="/profile/" render={() => (this.props.authorized) ? <Profile /> : <Redirect to='/login/' />} />
						<Route path="/search/" render={() => <Search />} />
						<Route path="/software/:developer/:product/" render={() => <SoftInfo />} />
						<Route path="/basket/" render={() => <Basket />} />
						<Route path="/checkout/" render={() => (this.props.authorized) ? <Checkout /> : <Redirect to='/basket/' />} />
					</Switch>
					<Footer />
				</BrowserRouter>
			</div>
		);
	}
}

export default App;
