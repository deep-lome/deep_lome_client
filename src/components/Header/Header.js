import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Header.css';

class Header extends Component {
	state = {
		searchValue: '',
	};

	componentDidUpdate() {
		(!this.props.search && window.location.href.includes('search')) && this.props.getSearchValue(this.state.searchValue);
	}

	handleChange = (value) => {
		value ? this.props.getSearchValue(value) : this.props.clearSearchValue();
		this.setState({ searchValue: value || '' });
	};

	render() {
		const categories = [...new Set(this.props.software.map(product => product.category))];
		return (
			<div className={styles.header}>
				<Link to='/catalog/' className={`${styles.catalog} ${styles.button}`}>
					<div className={`${styles.catalog_active} ${styles.alert} ${styles.active} ${styles.menu}`}>
						Перейти в каталог
					</div>
				</Link>
				<div className={styles.list}>
					<div className={`${styles.list_bar} ${styles.button}`} />
					<div className={styles.list_title}>Категории</div>
					<div className={`${styles.list_active} ${styles.active} ${styles.menu}`}>
						{categories.map((item, index) => <Link
							key={index} to='/search/'
							className={`${styles.item} ${styles.list_item}`}
							onClick={() => this.props.getCategoryValue(item)}>
							{item}
						</Link>)}
					</div>
				</div>
				<div className={styles.search}>
					<Link to='/search/' className={styles.search_button} />
					<div className={styles.search_line}>
						<input className={styles.input} type='text' placeholder='Поиск...'
							onChange={(e) => this.handleChange(e.target.value)}
							value={this.props.search} />
						<div className={styles.cancel_search}
							onClick={() => this.handleChange()} />
					</div>
				</div>
				<div className={`${styles.profile} ${styles.button}`}>
					<div className={`${styles.profile_active} ${styles.active} ${styles.menu}`}>
						<Link to={this.props.authorized ? '/profile/' : '/login/'} className={`${styles.item} ${styles.profile_item}`}>
							{this.props.authorized ? 'Профиль' : 'Авторизация'}
						</Link>
						<Link to={this.props.authorized ? '#' : '/registration/'} className={`${styles.item} ${styles.profile_item}`}
							onClick={this.props.authorized ? this.props.signOut : () => { }}>
							{this.props.authorized ? 'Выйти' : 'Регистрация'}
						</Link>
					</div>
				</div>
				<Link to={(this.props.content.length > 0) ? '/basket/' : '#'} className={`${styles.basket} ${styles.button}`}>
					{(this.props.content.length === 0) && <div className={`${styles.basket_active} ${styles.alert} ${styles.active} ${styles.menu}`}>
						Корзина пуста
					</div>}
					{this.props.size && <div className={styles.size}>
						{this.props.size}
					</div>}
				</Link>
			</div>
		);
	}
}

export default Header;
