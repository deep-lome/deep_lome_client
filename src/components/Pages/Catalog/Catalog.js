import React, { Component } from 'react';
import styles from './Catalog.css';
import SoftList from './Software/SoftList/SoftList';
import Numbering from '../../Common/Numbering/Numbering';
import DevList from './Software/DevList/DevList';

class Catalog extends Component {
	state = {
		page: 1,
	}

	handleClick = (page) => {
		this.setState({
			page,
		});
	}

	componentWillUnmount() {
		this.props.clearFiltersList();
	}

	getProductsForPage = () => this.props.software
		.filter(item => item.name.toLowerCase().startsWith(this.props.search.toLowerCase()))
		.filter(item => this.props.developer ? item.developer === this.props.developer : item);

	lenght = Math.ceil(this.getProductsForPage().length / 10);

	getArray = () => this.getProductsForPage()
		.slice(10 * (this.state.page - 1), 10 * (this.state.page - 1) + 10);

	render() {
		return (
			<div className={styles.catalog}>
				<DevList />
				<SoftList
					software={this.getArray()}
					search={this.props.search}
					page={this.state.page}
					lenght={this.lenght}
					changePage={(page) => this.handleClick(page)}
				/>
				<Numbering
					shadow={true}
					page={this.state.page}
					length={this.lenght}
					changePage={(page) => this.handleClick(page)}
				/>
			</div>
		);
	}
}

export default Catalog;
