import React from 'react';
import styles from './SoftList.css';
import Product from '../Product/Product';

const SoftList = ({ software, search, page, lenght, changePage }) => {
	const handleClick = (value) => {
		(value > 0) && (value < lenght + 1) && changePage(value);
	};

	return (
		<div className={styles.soft_list}>
			<div className={`${styles.pagination}`}>
				<div className={styles.wrap}>
					<div className={`${styles.button} ${styles.prev_page}`}
						onClick={() => handleClick(page - 1)} />
				</div>
				<div className={styles.wrap}>
					<div className={`${styles.button} ${styles.next_page}`}
						onClick={() => handleClick(page + 1)} />
				</div>
			</div>
			<div className={styles.list}>
				{search
					? (software.length !== 0)
						? software.map((product, index) =>
							<Product key={index} product={product} />)
						: <div className={styles.status}>
							Отсутствуют товары, удовлетворяющие условиям поиска
						</div>
					: software.map((product, index) =>
						<Product key={index} product={product} />)}
			</div>
		</div>
	);
};

export default SoftList;
