import React, { Component } from 'react';
import styles from './DevList.css';

class DevList extends Component {
	software = (this.props.software.length > 0) ? this.props.software : JSON.parse(localStorage.getItem('software'));

	state = {
		developers: [...new Set(this.software.map(product => product.developer))],
	};

	moveArray = (move, array, steps) => {
		if (move === 'left') {
			(steps === 2) ? array.unshift(array[array.length - 2], array[array.length - 1])
				: array.unshift(array[array.length - 1]);
		} else {
			(steps === 2) ? array.push(array[0], array[1])
				: array.push(array[0]);
		}
		for (let i = 0; i < steps; i++) {
			(move === 'left') ? array.pop() : array.shift();
		}
		return array;
	};

	selectDeveloper = (index, move, steps) => {
		(index === 2 && this.props.developer) ? this.props.clearDeveloper()
			: (index) && this.props.getDeveloper(this.state.developers[index]);
		move && this.setState({
			...this.state,
			developers: this.moveArray(move, this.state.developers, steps),
		});
	};

	render() {
		return (
			<div className={styles.dev_list}>
				<div className={`${styles.button} ${styles.left}`}
					onClick={() => this.selectDeveloper(this.props.developer ? 1 : false, 'left', 1)} />
				<div className={`${styles.list_item} ${styles.dev}`}
					onClick={() => this.selectDeveloper(0, 'left', 2)}>
					{this.state.developers[0]}</div>
				<div className={`${styles.list_item} ${styles.dev}`}
					onClick={() => this.selectDeveloper(1, 'left', 1)}>
					{this.state.developers[1]}</div>
				<div className={`${styles.list_item} ${styles.dev} ${this.props.developer && styles.active}`}
					onClick={() => this.selectDeveloper(2)}>
					{this.state.developers[2]}</div>
				<div className={`${styles.list_item} ${styles.dev}`}
					onClick={() => this.selectDeveloper(3, 'right', 1)}>
					{this.state.developers[3]}</div>
				<div className={`${styles.list_item} ${styles.dev}`}
					onClick={() => this.selectDeveloper(4, 'right', 2)}>
					{this.state.developers[4]}</div>
				<div className={`${styles.button} ${styles.right}`}
					onClick={() => this.selectDeveloper(this.props.developer ? 3 : false, 'right', 1)} />
			</div>
		);
	}
}

export default DevList;
