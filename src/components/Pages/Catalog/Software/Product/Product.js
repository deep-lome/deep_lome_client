import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import styles from './Product.css';

const Product = ({ product, getDeveloper }) => {
	const initialState = {
		active: false,
	};

	const [state, setState] = useState(initialState);

	const visibilityChange = (status) => {
		setState({ active: status });
	};

	const getString = (string) => string.replace(/ /g, '_');

	return (
		<div className={styles.soft}
			onMouseOver={() => visibilityChange(true)}
			onMouseOut={() => visibilityChange(false)}>
			<Link className={`${styles.soft_header}`} to={`/software/${getString(product.developer)}/${getString(product.name)}/`}>
				<img className={`${styles.soft_img}`} src={product.url} alt='Проблема с источником изображения' />
				<div className={`${styles.soft_name}`}>{product.name}</div>
			</Link>
			<Link className={`${styles.soft_developer} ${styles.row}`} to='/search/' onClick={() => getDeveloper(product.developer)}>{product.developer}</Link>
			<Link className={`${styles.soft_description} ${styles.row}`} to={`/software/${getString(product.developer)}/${getString(product.name)}/#description`}>{product.description}</Link>
			<Link className={`${styles.soft_price} ${state.active && styles.active}`}
				to={`/software/${getString(product.developer)}/${getString(product.name)}/#offers`}>
				{(product.offers.length > 1)
					? <Fragment>
						<span className={styles.price_text}>{'от'}</span>
						<span className={styles.price_value}>
							{Math.min(...product.offers.map(item => item.price)).toLocaleString()}
						</span>
					</Fragment>
					: <span className={styles.price_value}>
						{product.offers[0].price.toLocaleString()}
					</span>}
				<span>{'руб'}</span>
			</Link>
		</div>
	);
};

export default Product;
