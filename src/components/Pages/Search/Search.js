import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Search.css';
import Checkbox from '../../Common/Checkbox/Checkbox';
import Numbering from '../../Common/Numbering/Numbering';

class Search extends Component {
	componentWillUnmount() {
		this.props.clearFiltersList();
	}

	software = (this.props.software.length > 0) ? this.props.software : JSON.parse(localStorage.getItem('software'));

	developers = [...new Set(this.software.map(product => product.developer))];

	categories = [...new Set(this.software.map(product => product.category))];

	initialState = {
		lowerThreshold: false,
		highThreshold: false,
		checkedDevelopers: this.developers.map(item => this.props.developer
			? (item === this.props.developer)
			: false),
		checkedCategories: this.categories.map(item => this.props.category
			? (item === this.props.category)
			: false),
		isActive: [true, true, true],
		isShowing: [false],
		page: 1,
		volume: 5,
	}

	state = this.initialState;

	checkAvailability = (value, array, checked) => array
		.filter((_item, index) => checked[index]).includes(value);

	filterProducts = () => this.props.software
		.filter(item => item.name.toLowerCase().startsWith(this.props.search.toLowerCase()))
		.filter(item => this.state.checkedDevelopers.includes(true)
			? this.checkAvailability(item.developer, this.developers, this.state.checkedDevelopers)
			: item)
		.filter(item => this.state.checkedCategories.includes(true)
			? this.checkAvailability(item.category, this.categories, this.state.checkedCategories)
			: item)
		.filter(item => (this.filterOffers(item.offers)));

	filterOffers = (offers) => {
		const array = offers.filter(item => this.state.lowerThreshold
			? (this.state.lowerThreshold <= item.price)
			: item)
			.filter(item => this.state.highThreshold
				? (this.state.highThreshold >= item.price)
				: item);
		return array.length === 0 ? false : array;
	}

	getArray = () => this.filterProducts()
		.slice(
			this.state.volume * (this.state.page - 1),
			this.state.volume * (this.state.page - 1) + this.state.volume,
		);

	getString = (string) => string.replace(/ /g, '_');

	clearFilters = () => {
		this.props.clearFiltersList('search');
		this.setState({
			...this.initialState,
			checkedDevelopers: this.state.checkedDevelopers.map(() => false),
			checkedCategories: this.state.checkedCategories.map(() => false),
		});
	};

	hanleClick = (offer) => {
		this.props.content.some(item => item.product.name === offer.name)
			? this.props.changeBasketContent({
				size: this.props.size + 1,
				content: this.props.content.map(item => (item.product.name === offer.name)
					? {
						...item,
						count: item.count + 1,
					} : item),
			})
			: this.props.changeBasketContent({
				size: this.props.size + 1,
				content: [
					...this.props.content,
					{
						product: offer,
						count: 1,
					},
				],
			});
		this.props.showModalWindow('dialog');
	};

	handleChange = ({ name, value }) => {
		console.log(value);
		this.setState({
			...this.state,
			[name]: value,
		});
	};

	render() {
		return (
			<div className={styles.search}>
				<div className={`${styles.filters} ${styles.column}`}>
					<div className={styles.filters_head}>
						<span className={styles.filters_icon} />
						<span className={styles.filters_title}>Подбор параметров</span>
					</div>
					<div className={styles.filters_body}>
						<div className={styles.filter}>
							<div className={styles.filter_head} onClick={() => this.handleChange({
								name: 'isActive',
								value: this.state.isActive.map((item, index) => (index === 0)
									? !this.state.isActive[0]
									: item),
							})}>
								<div className={styles.filter_name}>Цена, ₽</div>
								<div className={`${styles.dropdown} ${!this.state.isActive[0] ? styles.dropdown_hidden : ''}`} />
							</div>
							<div className={`${styles.wrap} ${styles.transform} ${!this.state.isActive[0] ? styles.hidden_items : ''}`}>
								<div className={`${styles.lower_price} ${styles.form}`}>
									<label className={`${styles.label} ${styles.from}`} htmlFor='lowerThreshold'>от</label>
									<input
										id='lowerThreshold'
										name='lowerThreshold'
										className={styles.input}
										type='number'
										value={this.state.lowerThreshold || ''}
										onChange={(e) => this.handleChange(e.target)}>
									</input>
								</div>
								<div className={`${styles.high_price} ${styles.form}`}>
									<label className={`${styles.label} ${styles.to}`} htmlFor='highThreshold'>до</label>
									<input
										id='highThreshold'
										name='highThreshold'
										className={styles.input}
										type='number'
										value={this.state.highThreshold || ''}
										onChange={(e) => this.handleChange(e.target)}
									/>
								</div>
							</div>
						</div>
						<div className={styles.filter}>
							<div className={styles.filter_head} onClick={() => this.handleChange({
								name: 'isActive',
								value: this.state.isActive.map((item, index) => (index === 1)
									? !this.state.isActive[1]
									: item),
							})}>
								<div className={styles.filter_name}>Категория</div>
								<div className={`${styles.dropdown} ${!this.state.isActive[1] ? styles.dropdown_hidden : ''}`} />
							</div>
							<div className={`${styles.filter_items} ${styles.transform} ${!this.state.isActive[1] ? styles.hidden_items : ''}`}>
								{this.categories.map((_item, index, array) =>
									<div className={styles.checkbox} key={index}><Checkbox
										string={array[index]}
										size='small'
										isActive={this.state.checkedCategories[index]}
										markBox={() => this.handleChange({
											name: 'checkedCategories',
											value: this.state.checkedCategories
												.map((item, id) => (index === id)
													? !this.state.checkedCategories[index]
													: item),
										})}>
									</Checkbox></div>)}
							</div>
						</div>
						<div className={styles.filter}>
							<div className={styles.filter_head} onClick={() => this.handleChange({
								name: 'isActive',
								value: this.state.isActive.map((item, index) => (index === 2)
									? !this.state.isActive[2]
									: item),
							})}>
								<div className={styles.filter_name}>Производитель</div>
								<div className={`${styles.dropdown} ${!this.state.isActive[2] ? styles.dropdown_hidden : ''}`} />
							</div>
							<div className={`${styles.filter_items} ${styles.transform} ${!this.state.isActive[2] ? styles.hidden_items : ''}`}>
								{this.developers.map((_item, index, array) =>
									<div className={styles.checkbox} key={index}><Checkbox
										string={array[index]}
										size='small'
										isActive={this.state.checkedDevelopers[index]}
										markBox={() => this.handleChange({
											name: 'checkedDevelopers',
											value: this.state.checkedDevelopers
												.map((item, id) => (index === id)
													? !this.state.checkedDevelopers[index]
													: item),
										})}>
									</Checkbox></div>)}
							</div>
						</div>
						<div className={styles.filters_footer}>
							<div className={styles.filters_button} onClick={() => this.clearFilters()}>
								Сбросить фильтры
							</div>
						</div>
					</div>
				</div>
				<div className={`${styles.list} ${styles.column}`}>
					<div className={`${styles.list_head} ${this.filterProducts().length > 0 ? '' : styles.list_empty}`}>
						<div className={styles.list_title}>
							<div className={styles.title}>Поиск товаров</div>
							<div className={styles.count}>{this.filterProducts().length > 0 ? `${this.filterProducts().length} товар(ов)` : 'Ничего не найдено'}</div>
						</div>
						<div className={styles.list_buttons}>
							<div className={styles.volume_buttons}>
								<div className={styles.buttons_name}>Выводить по</div>
								<div className={styles.buttons}>
									<div className={`${styles.left_button} ${styles.button} ${(this.state.volume === 5) ? styles.button_active : ''}`}
										onClick={() => this.handleChange({ name: 'volume', value: 5 })}>
										5
									</div>
									<div className={`${styles.button} ${(this.state.volume === 10) ? styles.button_active : ''}`}
										onClick={() => this.handleChange({ name: 'volume', value: 10 })}>
										10
									</div>
									<div className={`${styles.right_button} ${styles.button} ${(this.state.volume === 20) ? styles.button_active : ''}`}
										onClick={() => this.handleChange({ name: 'volume', value: 20 })}>
										20
									</div>
								</div>
							</div>
							<div className={styles.visibility_buttons}>
								<div className={styles.buttons_name}>Показать</div>
								<div className={styles.buttons}>
									<div className={`${styles.left_button} ${styles.button} ${(this.state.isShowing.includes(true)) ? styles.button_active : ''}`}
										onClick={() => this.handleChange({
											name: 'isShowing',
											value: this.state.isShowing.length > 1
												? this.state.isShowing.map(() => true)
												: this.filterProducts().map(() => true),
										})}>
										<div className={`${styles.show} ${(this.state.isShowing.includes(true)) ? styles.visible : ''}`} />
									</div>
									<div className={`${styles.right_button} ${styles.button} ${(this.state.isShowing.includes(true)) ? '' : styles.button_active}`}
										onClick={() => this.handleChange({
											name: 'isShowing',
											value: this.state.isShowing.length > 1
												? this.state.isShowing.map(() => false)
												: this.filterProducts().map(() => false),
										})}>
										<div className={`${styles.hide} ${(this.state.isShowing.includes(true)) ? '' : styles.hidden}`} />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className={styles.list_body}>
						{(this.getArray().length !== 0)
							&& this.getArray().map((product, index) =>
								<div key={index} className={styles.product}>
									<div className={styles.product_head}>
										<Link to={`/software/${this.getString(product.developer)}/${this.getString(product.name)}/`}><img className={styles.product_img} src={product.url} /></Link>
										<div className={styles.product_title}>
											<Link className={styles.product_name} to={`/software/${this.getString(product.developer)}/${this.getString(product.name)}/`}>{product.name}</Link>
											<span className={styles.product_info}
												onClick={() => this.handleChange({
													name: 'checkedDevelopers',
													value: this.state.checkedDevelopers
														.map((item, id) => (this.developers[id] === product.developer)
															? true
															: item),
												})}>
												{product.developer}
											</span>
											<span className={styles.product_info}
												onClick={() => this.handleChange({
													name: 'checkedCategories',
													value: this.state.checkedCategories
														.map((item, id) => (this.categories[id] === product.category)
															? true
															: item),
												})}>
												{product.category}
											</span>
										</div>
										<div className={styles.product_button} onClick={() => this.handleChange({
											name: 'isShowing',
											value: this.state.isShowing.length > 1
												? this.state.isShowing.map((item, id) =>
													id === (index + (this.state.page - 1) * this.state.volume)
														? !this.state.isShowing[
															(index + (this.state.page - 1) * this.state.volume)]
														: item)
												: this.filterProducts().map((item, id) =>
													id === (index + (this.state.page - 1) * this.state.volume)
														? !this.state.isShowing[0]
														: false),
										})}>
											<div className={`${styles.visibility} ${(this.state.isShowing.length > 1
												? this.state.isShowing[(index + (this.state.page - 1) * this.state.volume)]
												: this.state.isShowing[0])
												? styles.hide_offers
												: styles.show_offers}`} />
										</div>
									</div>
									{(this.state.isShowing.length > 1
										? this.state.isShowing[(index + (this.state.page - 1) * this.state.volume)]
										: this.state.isShowing[0])
										&& <div className={styles.offers_list}>
											{product.offers.map((offer, id) => this.filterOffers([offer])
												&& <div key={id} className={styles.offer}>
													<Link className={styles.offer_title} to={`/software/${this.getString(product.developer)}/${this.getString(product.name)}/`}>{offer.name}</Link>
													<div className={styles.offer_content}>
														<div className={styles.offer_code}>{offer.code}</div>
														<div className={styles.offer_price} onClick={() =>
															this.hanleClick({
																url: product.url,
																name: `${product.name} ${offer.name}`,
																code: offer.code,
																price: offer.price,
																description: offer.properties,
															})}>
															<div className={styles.price}>
																<span className={styles.value}>
																	{offer.price.toLocaleString()}
																</span>
																<span>{' руб'}</span>
															</div>
															<div className={styles.offer_button}>
																<div className={styles.plus} />
															</div>
														</div>
													</div>
												</div>)}
										</div>}
								</div>)}
					</div>
					{((Math.ceil(this.filterProducts().length / this.state.volume) - 1) > 0)
						&& <div className={`${styles.pagination} ${this.state.isShowing[this.state.isShowing.length - 1] ? '' : styles.border}`}>
							<Numbering
								page={this.state.page}
								size='sm'
								length={Math.ceil(this.filterProducts().length / this.state.volume)}
								changePage={(page) => this.handleChange({ name: 'page', value: page })}
							/>
						</div>}
				</div>
			</div>
		);
	}
}

export default Search;
