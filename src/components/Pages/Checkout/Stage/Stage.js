import React, { Fragment } from 'react';
import styles from './Stage.css';

const Stage = ({ name, number, isActive }) => {
	return (
		<div className={styles.stage}>
			<svg className={`${styles.icon} ${(isActive >= number) ? styles.active_icon : ''}`} focusable="false" viewBox="0 0 24 24" aria-hidden="true">
				{(isActive > number)
					? <path d="M12 0a12 12 0 1 0 0 24 12 12 0 0 0 0-24zm-2 17l-5-5 1.4-1.4 3.6 3.6 7.6-7.6L19 8l-9 9z" />
					: <Fragment>
						<circle cx="12" cy="12" r="12"/>
						<text className={styles.icon_text} x="12" y="16" textAnchor="middle">{number}</text>
					</Fragment>}
			</svg>
			<span className={`${styles.stage_name} ${(isActive === number) ? styles.active_stage : ''}`}>{name}</span>
		</div>
	);
};

export default Stage;
