import CardInfo from 'card-info';
import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import styles from './Checkout.css';
import Input from '../../Common/Input/Input';
import Checkbox from '../../Common/Checkbox/Checkbox';
import Stage from './Stage/Stage';

const Checkout = ({ content, userInfo, sendOrder }) => {
	const initialState = {
		isActive: 1,
		stages: [
			'Контактные данные',
			'Детали платежа',
			'Сводка заказа',
		],
		inputValues: {
			firstName: userInfo.firstName || '',
			lastName: userInfo.lastName || '',
			phone: userInfo.phone || '',
			email: userInfo.email || '',
			cardName: '',
			cardNumber: '',
			expDate: '',
			cvv: '',
		},
		usingData: true,
		status: '',
		orderNumber: '',
	};
	const [state, setState] = useState(initialState);

	const handleChange = ({ name, value }) => {
		setState({
			...state,
			status: '',
			inputValues: {
				...state.inputValues,
				[name]: value,
			},
		});
	};

	const handleClick = (isActive, next) => {
		const array = Object.values({
			first: (state.isActive === 1) ? state.inputValues.firstName : state.inputValues.cardName,
			second: (state.isActive === 1) ? state.inputValues.lastName : state.inputValues.cardNumber,
			third: (state.isActive === 1) ? state.inputValues.phone : state.inputValues.expDate,
			fourth: (state.isActive === 1) ? state.inputValues.email : state.inputValues.cvv,
		});

		if (next && array.map(item => item === '').includes(true)) {
			setState({
				...state,
				status: 'Не все поля заполнены!',
			});
		} else if (next && array.map(item => item.startsWith(' ')).includes(true)) {
			setState({
				...state,
				status: 'Поля заполнены неправильно!',
			});
		} else if (next && state.isActive === 2
			&& (array[1].length < 16 || array[2].length < 7 || array[3].length < 3)) {
			setState({
				...state,
				status: 'Не все поля заполнены до конца!',
			});
		} else if (isActive === 4) {
			const orderNumber = Math.floor(Math.random() * (9999999999 - 1000000000) + 1000000000);
			setState({
				...state,
				status: '',
				isActive,
				orderNumber,
			});
			sendOrder({
				content: content.map(item => ({
					name: item.product.name,
					code: item.product.code,
					price: item.product.price,
					count: item.count,
				})),
				cost: content.map((item, index) => (item.product.price * content[index].count))
					.reduce((first, second) => first + second),
				orderNumber,
				customerInfo: {
					userName: userInfo.userName,
					firstName: state.inputValues.firstName,
					lastName: state.inputValues.lastName,
					phone: state.inputValues.phone,
					email: state.inputValues.email,
				},
				paymentData: {
					cardName: state.inputValues.cardName,
					cardNumber: state.inputValues.cardNumber,
					cardType: new CardInfo(state.inputValues.cardNumber).brandName,
					expDate: state.inputValues.expDate,
					cvv: state.inputValues.cvv,
				},
			});
		} else {
			setState({
				...state,
				status: '',
				isActive,
			});
		}
	};

	const useUserData = (usingData) => {
		usingData ? setState({
			...state,
			usingData,
			status: '',
			inputValues: {
				...state.inputValues,
				firstName: userInfo.firstName,
				lastName: userInfo.lastName,
				phone: userInfo.phone,
				email: userInfo.email,
			},
		}) : setState({
			...state,
			usingData,
			status: '',
			inputValues: {
				...state.inputValues,
				firstName: '',
				lastName: '',
				phone: '',
				email: '',
			},
		});
	};

	return (
		<div className={styles.checkout}>
			<div className={styles.body}>
				<div className={styles.title}>Оформление заказа</div>
				<div className={styles.header}>
					<Stage
						name='Контактные данные'
						number={1}
						isActive={state.isActive}
					/>
					<div className={styles.line} />
					<Stage
						name='Детали платежа'
						number={2}
						isActive={state.isActive}
					/>
					<div className={styles.line} />
					<Stage
						name='Просмотр деталей заказа'
						number={3}
						isActive={state.isActive}
					/>
				</div>
				<h6 className={styles.subtitle}>{initialState.stages[state.isActive - 1]}</h6>
				{state.status && <div className={styles.warning}>
					{state.status}
				</div>}
				{(state.isActive < 3) && <div className={styles.main}>
					{(state.isActive === 1) && <Fragment>
						<div className={styles.input}><Input
							name='firstName'
							type='text'
							placeholder='Имя'
							value={state.inputValues.firstName}
							isEmpty={state.status && (state.inputValues.firstName === '' || state.inputValues.firstName.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.input}><Input
							name='lastName'
							type='text'
							placeholder='Фамилия'
							value={state.inputValues.lastName}
							isEmpty={state.status && (state.inputValues.lastName === '' || state.inputValues.lastName.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.input}><Input
							name='phone'
							type='tel'
							placeholder='Номер телефона'
							value={state.inputValues.phone}
							isEmpty={state.status && (state.inputValues.phone === '' || state.inputValues.phone.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.input}><Input
							name='email'
							type='email'
							placeholder='Адрес электронной почты'
							value={state.inputValues.email}
							isEmpty={state.status && (state.inputValues.email === '' || state.inputValues.email.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.checkbox}>
							<Checkbox
								isActive={state.usingData}
								markBox={() => useUserData(!state.usingData)}
								string='Использовать данные, указанные при регистрации' />
						</div>
					</Fragment>}
					{(state.isActive === 2) && <Fragment>
						<div className={styles.input}><Input
							name='cardName'
							type='text'
							placeholder='Имя носителя карты'
							value={state.inputValues.cardName}
							isEmpty={state.status && (state.inputValues.cardName === '' || state.inputValues.cardName.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.input}><Input
							name='cardNumber'
							type='text'
							placeholder='Номер карты'
							value={state.inputValues.cardNumber}
							maxlength={16}
							notFilled={state.status && state.inputValues.cardNumber.length !== 16}
							isEmpty={state.status && (state.inputValues.cardNumber === '' || state.inputValues.cardNumber.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.input}><Input
							name='expDate'
							type='text'
							placeholder='Дата истечения срока действия (м/г)'
							value={state.inputValues.expDate}
							maxlength={7}
							notFilled={state.status && state.inputValues.expDate.length !== 7}
							isEmpty={state.status && (state.inputValues.expDate === '' || state.inputValues.expDate.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
						<div className={styles.input}><Input
							name='cvv'
							type='password'
							placeholder='CVV'
							helper="cvv-helper-text"
							value={state.inputValues.cvv}
							maxlength={3}
							notFilled={state.status && state.inputValues.cvv.length !== 3}
							isEmpty={state.status && (state.inputValues.cvv === '' || state.inputValues.cvv.startsWith(' '))}
							changeInputValue={(e) => handleChange(e.target)} /></div>
					</Fragment>}
				</div>}
				{(state.isActive === 3) && <Fragment>
					<ul className={styles.list}>
						{content.map((item, index) =>
							<li className={styles.item} key={index}>
								<div className={styles.product}>
									<span className={styles.name}>{`${item.product.name} (x${item.count})`}</span>
									<p className={styles.vendor_code}>{item.product.code}</p>
								</div>
								<p className={styles.price}>{`${(item.product.price * item.count).toLocaleString()} руб`}</p>
								<hr />
							</li>)}
						<li className={styles.item}>
							<div className={styles.total}>Итого</div>
							<h6 className={styles.summary}>{`${content.map((item, index) => (item.product.price * content[index].count))
								.reduce((first, second) => first + second).toLocaleString()} руб`}</h6>
						</li>
					</ul>
					<div className={styles.details}>
						<div className={styles.column}>
							<h6 className={styles.subtitle}>Контактные данные</h6>
							<p className={styles.text}>{`${state.inputValues.lastName} ${state.inputValues.firstName}`}</p>
							<p className={styles.text}>{state.inputValues.phone}</p>
							<p className={styles.text}>{state.inputValues.email}</p>
						</div>
						<div className={`${styles.column} ${styles.payment}`}>
							<h6 className={styles.subtitle}>Детали платежа</h6>
							<div className={styles.payment_details}>
								<p className={styles.block}>Тип карты</p>
								<p className={styles.block}>
									{new CardInfo(state.inputValues.cardNumber).brandName}
								</p>
								<p className={styles.block}>Имя держателя карты</p>
								<p className={styles.block}>{state.inputValues.cardName}</p>
								<p className={styles.block}>Номер карты</p>
								<p className={styles.block}>
									xxxx-xxxx-xxxx-{state.inputValues.cardNumber.slice(12, 16)}
								</p>
								<p className={styles.block}>Действует до</p>
								<p className={styles.block}>
									{state.inputValues.expDate.slice(0, 2)}/{state.inputValues.expDate.slice(3, 7)}
								</p>
							</div>
						</div>
					</div>
				</Fragment>}
				{(state.isActive > 3) && <Fragment>
					<h5 className={styles.subtitle_v2}>Благодарим вас за заказ</h5>
					<h6 className={styles.appeal}>
						<div>{`Номер вашего заказа #${state.orderNumber}.`}</div>
						<div>{`Для подтверждения указанной вами при оформлении заказа электронной почты мы отправили на неё детали вашего заказа.
						В скором времени наш оператор свяжется с вами по телефону для подтверждения заказа.
						После оплаты заказа на указанную вами почту мы вышлем данные для получения и активации приобретённого вами программного обеспечения.`}</div>
					</h6>
				</Fragment>}
				<div className={styles.footer}>
					{(state.isActive === 1)
						&& <Link className={`${styles.button} ${styles.back}`} to='/basket/'>
							Вернуться в корзину
						</Link>}
					{(state.isActive > 1 && state.isActive < 4)
						&& <div className={`${styles.button} ${styles.back}`} onClick={() => handleClick(state.isActive - 1, false)}>
							Назад
						</div>}
					{(state.isActive < 3)
						&& <div className={`${styles.button} ${styles.next}`} onClick={() => handleClick(state.isActive + 1, true)}>
							Далее
						</div>}
					{(state.isActive === 3)
						&& <div className={`${styles.button} ${styles.next}`} onClick={() => handleClick(state.isActive + 1, true)}>
							Оформить заказ
						</div>}
					{(state.isActive === 4)
						&& <Link className={`${styles.button} ${styles.next}`} to='/catalog/'>
							Вернуться в каталог
						</Link>}
				</div>
			</div>
		</div>
	);
};

export default Checkout;
