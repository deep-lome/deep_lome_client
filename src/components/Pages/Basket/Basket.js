import { Table } from 'react-bootstrap';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import styles from './Basket.css';

const Basket = ({ authorized, content, size, changeContent, clearBasket }) => {
	const initialState = {
		count: (content.length > 0)
			? content.map(item => item.count)
			: JSON.parse(localStorage.getItem('basket')).content.map(item => item.count),
		status: '',
	};
	const [state, setState] = useState(initialState);

	const addContent = (product) => {
		changeContent({
			size: size + 1,
			content: content.map(soft => (soft.product.name === product.name) ? {
				...soft,
				count: soft.count + 1,
			} : soft),
		});
	};

	const removeContent = (product, status, index) => {
		if (status) {
			changeContent({
				size: (size !== 1) ? size - state.count.filter((_item, id) => id === index)[0] : null,
				content: content.filter(soft => soft.product.name !== product.name),
			});
			setState({
				count: state.count.filter((item, id) => id !== index),
			});
		} else {
			changeContent({
				size: (size !== 1) ? size - 1 : null,
				content: content.map(soft => (soft.product.name === product.name)
					? {
						...soft,
						count: soft.count - 1,
					} : soft),
			});
		}
	};

	const checkValue = (value) => (value >= 1 && value <= 99);

	const handleChange = (value, index, action) => {
		if (checkValue(value)) {
			(action === 'change') && changeContent({
				size: state.count.map((item, id) => id === index ? value : item)
					.reduce((first, second) => first + second),
				content: content.map(soft => (soft.product.name === content[index].product.name)
					? {
						...soft,
						count: value,
					} : soft),
			});
			(action === 'minus') && removeContent(content[index].product, false);
			(action === 'plus') && addContent(content[index].product);
			setState({
				count: state.count.map((item, id) => id === index ? value : item),
			});
		}
	};

	const handleClick = () => {
		if (!authorized) {
			setState({
				...state,
				status: 'Войдите в аккаунт, чтобы оформить заказ!',
			});
		}
	};

	return (
		<div className={styles.basket}>
			<div className={`${styles.title} ${(content.length !== 0) ? styles.full : ''}`}>Ваша корзина</div>
			{(content.length === 0)
				? <div className={styles.status}>
					Корзина пуста
				</div>
				: <Table responsive className={styles.table}>
					<thead>
						<tr className={styles.header}>
							<th>Наименование товара</th>
							<th className={styles.price}>Цена (ед/руб)</th>
							<th>Кол-во</th>
							<th>Стоимость</th>
							<th className={styles.clear_basket} onClick={() => clearBasket()}>
								Очистить корзину
							</th>
						</tr>
					</thead>
					<tbody className={styles.body}>
						{content.map((item, index) =>
							<tr key={index}>
								<td className={styles.name}>{item.product.name}</td>
								<td>
									<span className={styles.value}>
										{item.product.price.toLocaleString()}
									</span>
									<span className={styles.text}>{' руб'}</span>
								</td>
								<td>
									<div className={styles.count}>
										<div className={`${styles.minus} ${styles.count_button}`}
											onClick={() => handleChange(content[index].count - 1, index, 'minus')} />
										<input className={styles.count_value} min='1' type='number' maxLength='2'
											value={state.count[index] || 1}
											onChange={(e) => handleChange(Number(e.target.value), index, 'change')} />
										<div className={`${styles.plus} ${styles.count_button}`}
											onClick={() => handleChange(content[index].count + 1, index, 'plus')} />
									</div>
								</td>
								<td>
									<span className={styles.value}>
										{(item.product.price * (content[index].count || 1)).toLocaleString()}
									</span>
									<span className={styles.text}>{' руб'}</span>
								</td>
								<td className={styles.urn}
									onClick={() => removeContent(content[index].product, true, index)} />
							</tr>)}
						{(content.length === 0) && <div className={styles.status}>
							Корзина пуста...
						</div>}
					</tbody>
				</Table>}
			{(content.length !== 0) && <div className={styles.total_cost}>
				<div className={styles.total}>Итоговая стоимость:</div>
				<div className={styles.cost}>
					<span className={styles.value}>
						{content.map((item, index) => (item.product.price * content[index].count))
							.reduce((first, second) => first + second).toLocaleString()}
					</span>
					<span className={styles.text}>{' руб'}</span>
				</div>
			</div>}
			<div className={styles.footer}>
				<Link className={`${styles.home} ${styles.button}`} to='/catalog'>
					Вернуться в каталог
				</Link>
				<div className={styles.warning}>{state.status}</div>
				{(content.length !== 0) && <Link className={`${styles.checkout} ${styles.button}`} to={authorized ? '/checkout' : '#'}
					onClick={() => handleClick()}>
					Оформить заказ
				</Link>}
			</div>
		</div>
	);
};

export default Basket;
