import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import styles from './SoftInfo.css';

class SoftInfo extends Component {
	developer = this.props.match.params.developer;

	product = this.props.match.params.product;

	getString = (string) => string.replace('_', ' ');

	software = (this.props.software.length > 0) ? this.props.software : JSON.parse(localStorage.getItem('software'));

	soft = this.software.filter(item => (item.name === this.getString(this.product)))[0];

	getOffer = (offer) => ({
		url: this.soft.url,
		name: `${this.soft.name} ${offer.name}`,
		code: offer.code,
		price: offer.price,
		description: offer.properties,
	});

	handleClick = (offer) => {
		this.props.content.some(item => item.product.name === `${this.soft.name} ${offer.name}`)
			? this.props.changeBasketContent({
				size: this.props.size + 1,
				content: this.props.content.map(item => (item.product.name === `${this.soft.name} ${offer.name}`) ? {
					...item,
					count: item.count + 1,
				} : item),
			})
			: this.props.changeBasketContent({
				size: this.props.size + 1,
				content: [
					...this.props.content,
					{
						product: this.getOffer(offer),
						count: 1,
					},
				],
			});
		this.props.showModalWindow('dialog');
	};

	render() {
		return (
			<div className={styles.software}>
				<div className={styles.navigation}>
					<Link to='/catalog/' className={styles.link}>Каталог товаров</Link>
					<div className={styles.delimiter} />
					<Link to='/search/' className={styles.link} onClick={() => this.props.getDeveloperValue(this.soft.developer)}>{this.soft.developer}</Link>
					<div className={styles.delimiter} />
					<Link to={`/software/${this.developer}/${this.product}/`} className={styles.link}>{this.soft.name}</Link>
				</div>
				<div className={styles.content}>
					<div className={styles.head}>
						<img className={styles.img} src={this.soft.url}/>
						<div className={styles.info}>
							<div className={styles.title}>{this.soft.name}</div>
							<div className={styles.row}>
								<span className={styles.subtitle}>Разработчик: </span>
								<Link to='/search/' className={styles.sublink} onClick={() => this.props.getDeveloperValue(this.soft.developer)}>{this.soft.developer}</Link>
							</div>
							<div className={styles.row}>
								<span className={styles.subtitle}>Категория: </span>
								<Link to='/search/' className={styles.sublink} onClick={() => this.props.getCategoryValue(this.soft.category)}>{this.soft.category}</Link>
							</div>
							<div className={styles.row}>
								<span className={styles.subtitle}>Доставка: </span>
								<span>Электронная (✉)</span>
							</div>
						</div>
					</div>
					<div className={styles.offers} id='offers'>
						{this.soft.offers.map((item, index) => <div key={index} className={styles.offer}>
							<div className={styles.offer_head}>
								<div className={styles.offer_name}>{item.name}</div>
								<div className={styles.offer_code}>{item.code}</div>
								<div className={styles.offer_price} onClick={() => this.handleClick(item)}>
									<div className={styles.price}>
										<span className={styles.value}>
											{item.price.toLocaleString()}
										</span>
										<span>{' руб'}</span>
									</div>
									<div className={styles.button}>
										<div className={styles.plus} />
									</div>
								</div>
							</div>
							<div className={styles.line} />
							<div className={styles.offer_description}>{item.properties}</div>
						</div>)}
					</div>
					<div className={styles.description} id='description'>
						{this.soft.description}
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(SoftInfo);
