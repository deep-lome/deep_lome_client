import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Login.css';
import Input from '../../../Common/Input/Input';
import Checkbox from '../../../Common/Checkbox/Checkbox';

class Login extends Component {
	state = {
		userName: '',
		password: '',
		rememberUser: false,
		status: '',
	};

	componentDidMount() {
		const rememberUser = localStorage.getItem('rememberUser') === 'true';
		const userName = rememberUser ? localStorage.getItem('userName') : '';
		this.setState({ ...this.state, userName, rememberUser });
	}

	componentDidUpdate() {
		if (this.state.status.length === 0 && this.props.status.length !== 0) {
			this.setState({
				...this.state,
				status: this.props.status,
			});
		}
		this.props.getStatusValue('');
	}

	componentWillUnmount() {
		this.props.getStatusValue('');
	}

	handleChange = ({ name, value }) => {
		this.setState({
			...this.state,
			status: '',
			[name]: value,
		});
	};

	onSubmit = (values) => {
		const { userName, rememberUser } = this.state;
		const array = Object.values(values);
		if (array.map(item => item === '').includes(true)) {
			this.handleChange({ name: 'status', value: 'Не все поля заполнены!' });
		} else if (array.map(item => item.startsWith(' ')).includes(true)) {
			this.handleChange({ name: 'status', value: 'Поля заполнены неправильно!' });
		} else {
			localStorage.setItem('rememberUser', rememberUser);
			localStorage.setItem('userName', rememberUser ? userName : '');
			this.props.signIn(values);
			this.handleChange({ name: 'status', value: '' });
		}
	};

	render() {
		return (
			<div className={styles.background}>
				<div className={styles.authorization}>
					<h1 className={styles.title}>Авторизация</h1>
					{this.state.status
						&& <div className={styles.status}>
							{this.state.status}
						</div>}
					<form className={styles.body} noValidate>
						<Input
							name='userName'
							type='text'
							placeholder='Логин'
							value={this.state.userName}
							autofocus={true}
							size='big'
							isEmpty={this.state.status && (this.state.userName === '' || this.state.userName.startsWith(' '))}
							changeInputValue={(e) => this.handleChange(e.target)} />
						<Input
							name='password'
							type='password'
							placeholder='Пароль'
							value={this.state.password}
							size='big'
							isEmpty={this.state.status && (this.state.password === '' || this.state.password.startsWith(' '))}
							changeInputValue={(e) => this.handleChange(e.target)} />
						<Checkbox
							string='Запомнить пользователя'
							isActive={this.state.rememberUser}
							size='big'
							markBox={() => this.handleChange({ name: 'rememberUser', value: !this.state.rememberUser })} />
					</form>
					<div className={styles.submit} onClick={() => this.onSubmit({
						userName: this.state.userName,
						password: this.state.password,
					})}>
						Войти в аккаунт
					</div>
					<div className={styles.footer}>
						<Link className={styles.link} to='/registration/'>Нет аккаунта?</Link>
					</div>
				</div>
			</div>
		);
	}
}

export default Login;
