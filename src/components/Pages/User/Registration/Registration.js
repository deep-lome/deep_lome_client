import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Registration.css';
import Input from '../../../Common/Input/Input';

class Registration extends Component {
	state = {
		userName: '',
		firstName: '',
		lastName: '',
		phone: '',
		email: '',
		password: '',
		confirmPassword: '',
		status: '',
	};

	componentDidUpdate() {
		if (this.state.status.length === 0 && this.props.status.length !== 0) {
			this.setState({
				...this.state,
				status: this.props.status,
			});
			this.props.getStatusValue('');
		}
	}

	handleChange = ({ name, value }) => {
		this.setState({
			...this.state,
			status: '',
			[name]: value,
		});
	};

	onSubmit = (userInfo) => {
		const array = Object.values(userInfo);
		if (array.map(item => item === '').includes(true)) {
			this.handleChange({ name: 'status', value: 'Не все поля заполнены!' });
		} else if (array.map(item => item.startsWith(' ')).includes(true)) {
			this.handleChange({ name: 'status', value: 'Поля заполнены неправильно!' });
		} else if (this.state.password !== this.state.confirmPassword) {
			this.handleChange({ name: 'status', value: 'Введённые пароли не совпадают!' });
		} else {
			this.props.signUp(userInfo);
			this.handleChange({ name: 'status', value: '' });
		}
	};

	render() {
		return (
			<div className={styles.background}>
				<div className={styles.registration}>
					<h1 className={styles.title}>Регистрация</h1>
					{this.state.status && <div className={`${styles.status} ${this.state.status === 'success' ? styles.success : ''}`}>
						{this.state.status === 'success'
							? 'Вы успешно зарегистрировались!'
							: this.state.status}
					</div>}
					<form className={styles.body} noValidate>
						{this.state.status !== 'success' && <div className={styles.input_list}>
							<div className={`${styles.input_wrap} ${styles.full}`}><Input
								name='userName'
								type='text'
								placeholder='Логин'
								value={this.state.userName}
								autofocus={true}
								size='middle'
								isEmpty={this.state.status && (this.state.userName === '' || this.state.userName.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
							<div className={`${styles.input_wrap} ${styles.half}`}><Input
								name='firstName'
								type='text'
								placeholder='Имя'
								value={this.state.firstName}
								size='middle'
								isEmpty={this.state.status && (this.state.firstName === '' || this.state.firstName.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
							<div className={`${styles.input_wrap} ${styles.half}`}><Input
								name='lastName'
								type='text'
								placeholder='Фамилия'
								value={this.state.lastName}
								size='middle'
								isEmpty={this.state.status && (this.state.lastName === '' || this.state.lastName.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
							<div className={`${styles.input_wrap} ${styles.full}`}><Input
								name='phone'
								type='tel'
								placeholder='Номер телефона'
								value={this.state.phone}
								size='middle'
								isEmpty={this.state.status && (this.state.phone === '' || this.state.phone.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
							<div className={`${styles.input_wrap} ${styles.full}`}><Input
								name='email'
								type='email'
								placeholder='Адрес электронной почты'
								value={this.state.email}
								size='middle'
								isEmpty={this.state.status && (this.state.email === '' || this.state.email.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
							<div className={`${styles.input_wrap} ${styles.full}`}><Input
								name='password'
								type='password'
								placeholder='Пароль'
								value={this.state.password}
								size='middle'
								isEmpty={this.state.status && (this.state.password === '' || this.state.password.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
							<div className={`${styles.input_wrap} ${styles.full}`}><Input
								name='confirmPassword'
								type='password'
								placeholder='Подтвердите пароль'
								value={this.state.confirmPassword}
								size='middle'
								isEmpty={this.state.status && (this.state.confirmPassword === '' || this.state.confirmPassword.startsWith(' '))}
								changeInputValue={(e) => this.handleChange(e.target)} /></div>
						</div>}
						<Link className={styles.submit} onClick={(this.state.status !== 'success') ? () => this.onSubmit({
							userName: this.state.userName,
							firstName: this.state.firstName,
							lastName: this.state.lastName,
							phone: this.state.phone,
							email: this.state.email,
							password: this.state.password,
						}) : () => { }} to={(this.state.status === 'success') ? '/login/' : '#'}>
							{(this.state.status !== 'success') ? 'Зарегистрироваться' : 'Войти в аккаунт'}
						</Link>
						{(this.state.status !== 'success') && <div className={styles.footer}>
							<Link to='/login/'>Уже есть аккаунт?</Link>
						</div>}
					</form>
				</div>
			</div>
		);
	}
}

export default Registration;
