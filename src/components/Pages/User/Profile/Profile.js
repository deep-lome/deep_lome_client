import { Table } from 'react-bootstrap';
import React from 'react';
import styles from './Profile.css';

const Registration = ({ userInfo }) => (
	<div className={styles.background}>
		<div className={styles.profile}>
			<h1 className={styles.title}>Профиль</h1>
			<Table striped bordered className={styles.table}>
				<tbody className={styles.body}>
					<tr>
						<td className={styles.block}>
							Имя учётной записи
						</td>
						<td className={styles.block}>
							{userInfo.userName}
						</td>
					</tr>
					<tr>
						<td className={styles.block}>
							Полное имя пользователя
						</td>
						<td className={styles.block}>
							{`${userInfo.lastName} ${userInfo.firstName}`}
						</td>
					</tr>
					<tr>
						<td className={styles.block}>
							Адрес электронной почты
						</td>
						<td className={styles.block}>
							{userInfo.email}
						</td>
					</tr>
					<tr>
						<td className={styles.block}>
							Номер телефона
						</td>
						<td className={styles.block}>
							{userInfo.phone}
						</td>
					</tr>
				</tbody>
			</Table>
		</div>
	</div>
);

export default Registration;
