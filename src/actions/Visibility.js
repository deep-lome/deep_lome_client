import { ACTIONS } from '../utils/constants';

export const showModalWindow = (payload) => ({
	type: ACTIONS.SHOW_MODAL_WINDOW,
	payload,
});

export const hideModalWindow = () => ({
	type: ACTIONS.HIDE_MODAL_WINDOW,
});
