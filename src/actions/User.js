import { ACTIONS } from '../utils/constants';

export const fillUserInfo = (payload) => ({
	type: ACTIONS.FILL_USER_INFO,
	payload,
});

export const getStatusValue = (payload) => ({
	type: ACTIONS.GET_STATUS_VALUE,
	payload,
});

export const clearUserInfo = () => ({
	type: ACTIONS.CLEAR_USER_INFO,
});
