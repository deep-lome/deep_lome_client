import { ACTIONS } from '../utils/constants';

export const clearLocalBasketContent = () => ({
	type: ACTIONS.CLEAR_BASKET,
});

export const changeLocalBasketContent = (payload) => ({
	type: ACTIONS.CHANGE_BASKET_CONTENT,
	payload,
});
