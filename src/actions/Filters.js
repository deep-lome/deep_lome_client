import { ACTIONS } from '../utils/constants';

export const getSearchValue = (payload) => ({
	type: ACTIONS.GET_SEARCH_VALUE,
	payload,
});

export const clearSearchValue = () => ({
	type: ACTIONS.CLEAR_SEARCH_VALUE,
});

export const getDeveloperValue = (payload) => ({
	type: ACTIONS.GET_DEVELOPER_VALUE,
	payload,
});

export const clearDeveloperValue = () => ({
	type: ACTIONS.CLEAR_DEVELOPER_VALUE,
});

export const getCategoryValue = (payload) => ({
	type: ACTIONS.GET_CATEGORY_VALUE,
	payload,
});

export const clearCategoryValue = () => ({
	type: ACTIONS.CLEAR_CATEGORY_VALUE,
});

export const clearFiltersList = (payload) => ({
	type: ACTIONS.CLEAR_FILTERS,
	payload,
});
