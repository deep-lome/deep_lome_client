import { ACTIONS } from '../utils/constants';

export const fillSoftwareList = (payload) => ({
	type: ACTIONS.FILL_SOFTWARE_LIST,
	payload,
});
